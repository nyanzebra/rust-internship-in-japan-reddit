use chrono::*;
use serde_derive::*;

macro_rules! impl_from_string_for {
    ($type:ty, $block:block, $value:ident) => (
        impl From<$type> for String {
            fn from($value: $type) -> String $block
        }
        impl<'a> From<&'a $type> for String {
            fn from($value: &'a $type) -> String $block
        }
    );
}

pub fn byte_array_to_string(arr: &[u8]) -> String {
    let mut res = String::new();
    for byte in arr {
        res.push(*byte as char);
    }
    res
}

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct U64ByteArray([u8; 4]);

impl From<U64ByteArray> for u64 {
    fn from(value: U64ByteArray) -> u64 {
        let mut res = 0u64;
        res |= value.0[0] as u64;
        res <<= 8;
        res |= value.0[1] as u64;
        res <<= 8;
        res |= value.0[2] as u64;
        res <<= 8;
        res |= value.0[3] as u64;
        res
    }
}

impl<'a> From<&'a U64ByteArray> for u64 {
    fn from(value: &'a U64ByteArray) -> u64 {
        let mut res = 0u64;
        res |= value.0[0] as u64;
        res <<= 8;
        res |= value.0[1] as u64;
        res <<= 8;
        res |= value.0[2] as u64;
        res <<= 8;
        res |= value.0[3] as u64;
        res
    }
}

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct UtcTime(u64);

impl From<u64> for UtcTime {
    fn from(value: u64) -> Self {
        Self(value)
    }
}
impl<'a> From<&'a u64> for UtcTime {
    fn from(value: &'a u64) -> Self {
        Self(*value)
    }
}

impl_from_string_for!(
    UtcTime,
    {
        let date = DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(value.0 as i64, 0), Utc);
        date.to_rfc3339()
    },
    value
);

impl_from_string_for!(U64ByteArray, { format!("{}", u64::from(value)) }, value);
