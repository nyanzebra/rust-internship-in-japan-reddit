use crate::util::{byte_array_to_string, U64ByteArray};
use getset::*;
use serde_derive::*;
use static_assertions::*;

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Price([u8; 5]);

assert_eq_size!(Price, [u8; 5]);

impl_from_string_for!(Price, { byte_array_to_string(&value.0) }, value);

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Quantity([u8; 7]);

assert_eq_size!(Quantity, [u8; 7]);

impl_from_string_for!(Quantity, { byte_array_to_string(&value.0) }, value);

#[derive(Debug, Serialize, Deserialize, Getters, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Ask {
    #[get = "pub"]
    price: Price,
    #[get = "pub"]
    quantity: Quantity,
}

assert_eq_size!(Ask, [u8; 12]);

impl_from_string_for!(
    Ask,
    {
        format!(
            "{:?}, {:?}",
            String::from(value.price()),
            String::from(value.quantity())
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Asks {
    total_asks_volume: [u8; 7],
    asks: [Ask; 5],
}

assert_eq_size!(Asks, [u8; 67]);

impl_from_string_for!(
    Asks,
    {
        format!(
            "{}, [{}, {}, {}, {}, {}]",
            byte_array_to_string(&value.total_asks_volume),
            String::from(&value.asks[0]),
            String::from(&value.asks[1]),
            String::from(&value.asks[2]),
            String::from(&value.asks[3]),
            String::from(&value.asks[4]),
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct AskQuotes {
    number_of_best_ask_valid_quote_total: [u8; 5],
    number_of_best_ask_valid_quote_1st: U64ByteArray,
    number_of_best_ask_valid_quote_2nd: U64ByteArray,
    number_of_best_ask_valid_quote_3rd: U64ByteArray,
    number_of_best_ask_valid_quote_4th: U64ByteArray,
    number_of_best_ask_valid_quote_5th: U64ByteArray,
}

assert_eq_size!(AskQuotes, [u8; 25]);

impl_from_string_for!(
    AskQuotes,
    {
        format!(
            "{:?}, [{:?}, {:?}, {:?}, {:?}, {:?}]",
            byte_array_to_string(&value.number_of_best_ask_valid_quote_total),
            String::from(&value.number_of_best_ask_valid_quote_1st),
            String::from(&value.number_of_best_ask_valid_quote_2nd),
            String::from(&value.number_of_best_ask_valid_quote_3rd),
            String::from(&value.number_of_best_ask_valid_quote_4th),
            String::from(&value.number_of_best_ask_valid_quote_5th),
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Getters, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Bid {
    #[get = "pub"]
    price: Price,
    #[get = "pub"]
    quantity: Quantity,
}

assert_eq_size!(Bid, [u8; 12]);

impl_from_string_for!(
    Bid,
    {
        format!(
            "{:?}, {:?}",
            String::from(value.price()),
            String::from(value.quantity())
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct Bids {
    total_bids_volume: [u8; 7],
    bids: [Bid; 5],
}

assert_eq_size!(Bids, [u8; 67]);

impl_from_string_for!(
    Bids,
    {
        format!(
            "{}, [{:?}, {:?}, {:?}, {:?}, {:?}]",
            byte_array_to_string(&value.total_bids_volume),
            String::from(&value.bids[0]),
            String::from(&value.bids[1]),
            String::from(&value.bids[2]),
            String::from(&value.bids[3]),
            String::from(&value.bids[4]),
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct BidQuotes {
    number_of_best_bid_valid_quote_total: [u8; 5],
    number_of_best_bid_valid_quote_1st: U64ByteArray,
    number_of_best_bid_valid_quote_2nd: U64ByteArray,
    number_of_best_bid_valid_quote_3rd: U64ByteArray,
    number_of_best_bid_valid_quote_4th: U64ByteArray,
    number_of_best_bid_valid_quote_5th: U64ByteArray,
}

assert_eq_size!(BidQuotes, [u8; 25]);

impl_from_string_for!(
    BidQuotes,
    {
        format!(
            "{:?}, [{:?}, {:?}, {:?}, {:?}, {:?}]",
            byte_array_to_string(&value.number_of_best_bid_valid_quote_total),
            String::from(&value.number_of_best_bid_valid_quote_5th),
            String::from(&value.number_of_best_bid_valid_quote_4th),
            String::from(&value.number_of_best_bid_valid_quote_3rd),
            String::from(&value.number_of_best_bid_valid_quote_2nd),
            String::from(&value.number_of_best_bid_valid_quote_1st),
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct QuoteAcceptTime([u8; 8]);

impl From<QuoteAcceptTime> for u64 {
    fn from(value: QuoteAcceptTime) -> Self {
        let mut res = 0u64;
        res |= value.0[0] as u64;
        res <<= 8;
        res |= value.0[1] as u64;
        res <<= 8;
        res |= value.0[2] as u64;
        res <<= 8;
        res |= value.0[3] as u64;
        res <<= 8;
        res |= value.0[4] as u64;
        res <<= 8;
        res |= value.0[5] as u64;
        res <<= 8;
        res |= value.0[6] as u64;
        res <<= 8;
        res |= value.0[7] as u64;
        res
    }
}

impl<'a> From<&'a QuoteAcceptTime> for u64 {
    fn from(value: &'a QuoteAcceptTime) -> Self {
        let mut res = 0u64;
        res |= value.0[0] as u64;
        res <<= 8;
        res |= value.0[1] as u64;
        res <<= 8;
        res |= value.0[2] as u64;
        res <<= 8;
        res |= value.0[3] as u64;
        res <<= 8;
        res |= value.0[4] as u64;
        res <<= 8;
        res |= value.0[5] as u64;
        res <<= 8;
        res |= value.0[6] as u64;
        res <<= 8;
        res |= value.0[7] as u64;
        res
    }
}

impl_from_string_for!(
    QuoteAcceptTime,
    {
        format!(
            "{}{}:{}{}:{}{}.{}{}",
            value.0[0] as char,
            value.0[1] as char,
            value.0[2] as char,
            value.0[3] as char,
            value.0[4] as char,
            value.0[5] as char,
            value.0[6] as char,
            value.0[7] as char,
        )
    },
    value
);

// B6034 => 42,36,30,33,34
#[derive(Debug, Serialize, Deserialize, Getters, Hash, PartialEq, Eq, PartialOrd, Ord)]
pub struct QuotePacket {
    #[get = "pub"]
    data_type: [u8; 2], // B6
    #[get = "pub"]
    information_type: [u8; 2], // 03
    #[get = "pub"]
    market_type: u8, // 4
    #[get = "pub"]
    issue_code: [u8; 12],
    #[get = "pub"]
    issue_sequence_number: [u8; 3],
    #[get = "pub"]
    market_status_type: [u8; 2],
    #[get = "pub"]
    bids: Bids,
    #[get = "pub"]
    asks: Asks,
    #[get = "pub"]
    bid_quotes: BidQuotes,
    #[get = "pub"]
    ask_quotes: AskQuotes,
    #[get = "pub"]
    quote_accept_time: QuoteAcceptTime,
    #[get = "pub"]
    eom: u8,
}

assert_eq_size!(QuotePacket, [u8; 215]);

impl_from_string_for!(
    QuotePacket,
    {
        //<accept-time> <issue-code> <bqty5>@<bprice5> ... <bqty1>@<bprice1> <aqty1>@<aprice1> ... <aqty5>@<aprice5>
        format!(
            "{}, {}, {}, {}, {}, {}, {}",
            String::from(&value.quote_accept_time),
            // Can uncomment out for debugging
            // byte_array_to_string(&value.data_type),
            // byte_array_to_string(&value.information_type),
            // value.market_type,
            byte_array_to_string(&value.issue_code),
            // Can uncomment out for debugging
            // byte_array_to_string(&value.issue_sequence_number),
            // byte_array_to_string(&value.market_status_type),
            String::from(&value.bids),
            String::from(&value.asks),
            String::from(&value.bid_quotes),
            String::from(&value.ask_quotes),
            value.eom,
        )
        .chars()
        .filter(|&c| !"\\".contains(c))
        .collect()
    },
    value
);

pub const QUOTE_PACKET_SIZE: usize = std::mem::size_of::<QuotePacket>();

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_quote_packet_size() {
        assert_eq!(QUOTE_PACKET_SIZE, 215);
    }
}
