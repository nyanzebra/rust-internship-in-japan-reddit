use crate::util::{byte_array_to_string, U64ByteArray, UtcTime};

use getset::*;
use serde_derive::*;
use static_assertions::*;

#[derive(Debug, Serialize, Deserialize, Getters)]
pub struct PCAPHeader {
    #[get = "pub"]
    magic_number: U64ByteArray,
    #[get = "pub"]
    version_major: [u8; 2],
    #[get = "pub"]
    version_minor: [u8; 2],
    #[get = "pub"]
    thiszone: U64ByteArray,
    #[get = "pub"]
    sigfigs: U64ByteArray,
    #[get = "pub"]
    snaplen: U64ByteArray,
    #[get = "pub"]
    network: U64ByteArray,
}

assert_eq_size!(PCAPHeader, [u8; 24]);

impl_from_string_for!(
    PCAPHeader,
    {
        format!(
            "{} {} {} {} {} {} {}",
            String::from(&value.magic_number),
            byte_array_to_string(&value.version_major),
            byte_array_to_string(&value.version_minor),
            String::from(&value.thiszone),
            String::from(&value.sigfigs),
            String::from(&value.snaplen),
            String::from(&value.network)
        )
    },
    value
);

#[derive(Debug, Serialize, Deserialize, Getters)]
pub struct PCAPRecord {
    #[get = "pub"]
    ts_sec: U64ByteArray,
    #[get = "pub"]
    ts_usec: U64ByteArray,
    #[get = "pub"]
    incl_len: U64ByteArray,
    #[get = "pub"]
    orig_len: U64ByteArray,
}

assert_eq_size!(PCAPRecord, [u8; 16]);

impl_from_string_for!(
    PCAPRecord,
    {
        let utctime = UtcTime::from(u64::from(&value.ts_sec));
        format!(
            "{} {} {}",
            String::from(utctime),
            String::from(&value.incl_len),
            String::from(&value.orig_len),
        )
    },
    value
);
