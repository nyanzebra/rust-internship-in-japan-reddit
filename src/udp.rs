use getset::*;
use serde_derive::*;
use static_assertions::*;

#[derive(Debug, Serialize, Deserialize, Getters)]
pub struct UDPHeader {
    #[get = "pub"]
    destination: [u8; 6],
    #[get = "pub"]
    source: [u8; 6],
    #[get = "pub"]
    ip_type: [u8; 2],
    #[get = "pub"]
    ipv4_stuff: [u8; 20],
    #[get = "pub"]
    source_port: [u8; 2],
    #[get = "pub"]
    destination_port: [u8; 2],
    #[get = "pub"]
    length: [u8; 2],
    #[get = "pub"]
    checksum: [u8; 2],
}

assert_eq_size!(UDPHeader, [u8; 42]);
