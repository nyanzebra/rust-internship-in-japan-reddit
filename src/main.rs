use crate::{
    pcap::{PCAPHeader, PCAPRecord},
    quote_packet::{QuotePacket, QUOTE_PACKET_SIZE},
    udp::UDPHeader,
};
use clap::{load_yaml, App};
use priority_queue::PriorityQueue;
use serde::de::DeserializeOwned;
use std::{
    cmp::Reverse,
    fmt::Debug,
    fs::File,
    io::{BufReader, ErrorKind, Read},
    mem::size_of,
    rc::Rc,
};
use util::*;
#[macro_use]
mod util;
mod pcap;
mod quote_packet;
mod udp;

fn get_object<T>(buffer: &mut BufReader<File>) -> Result<Rc<T>, ErrorKind>
where
    T: Sized + DeserializeOwned + Debug,
{
    let mut data = Vec::new();
    data.resize(size_of::<T>(), 0);
    match buffer.read_exact(data.as_mut()) {
        Ok(_) => {
            let object: T = bincode::deserialize(data.as_mut()).unwrap();
            Ok(Rc::from(object))
        }
        Err(err) => Err(err.kind()),
    }
}

fn get_global_pcap_header(buffer: &mut BufReader<File>) -> Result<Rc<PCAPHeader>, ErrorKind> {
    get_object(buffer)
}

fn get_pcap_record(buffer: &mut BufReader<File>) -> Result<Rc<PCAPRecord>, ErrorKind> {
    get_object(buffer)
}

fn get_packet_metadata(buffer: &mut BufReader<File>) -> Result<Rc<UDPHeader>, ErrorKind> {
    get_object(buffer)
}

fn get_quote_packet(buffer: &mut BufReader<File>) -> Result<Rc<QuotePacket>, ErrorKind> {
    get_object(buffer)
}

fn add_quote_packet_to_priority_queue(
    priority_queue: &mut PriorityQueue<(Rc<QuotePacket>, UtcTime), usize>,
    quote_packet: Rc<QuotePacket>,
    time: UtcTime,
) {
    let priority = u64::from(quote_packet.quote_accept_time());
    priority_queue.push((quote_packet, time), Reverse(priority as usize).0);
}

fn main() {
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    let input_filename = matches.value_of("INPUT").unwrap();

    println!("Parsing market data from {}", input_filename);

    let mut should_reorder = false;
    if matches.is_present("reorder") {
        println!("Reordering market data based on quote accept time");
        should_reorder = true;
    }

    let input_file = File::open(input_filename).unwrap();

    let mut config = bincode::config();
    config.little_endian();

    let mut buffer = BufReader::new(input_file);
    let global_header = match get_global_pcap_header(&mut buffer) {
        Ok(res) => res,
        Err(errkind) => match errkind {
            ErrorKind::Interrupted => panic!("Interrupted IO!"),
            ErrorKind::UnexpectedEof => {
                println!("Reached EOF!");
                return;
            }
            _ => panic!("Unexpected Error!"),
        },
    };
    println!("pcap global header {:?}", global_header);
    let mut packet_count = 0u64;
    let mut quote_packet_count = 0u64;
    let mut quote_packet_priority_queue = PriorityQueue::new();
    loop {
        config.little_endian();
        let pcap = match get_pcap_record(&mut buffer) {
            Ok(res) => res,
            Err(errkind) => match errkind {
                ErrorKind::Interrupted => panic!("Interrupted IO!"),
                ErrorKind::UnexpectedEof => {
                    println!("Reached EOF!");
                    break;
                }
                _ => panic!("Unexpected Error!"),
            },
        };
        config.big_endian();
        let metadata = match get_packet_metadata(&mut buffer) {
            Ok(res) => res,
            Err(errkind) => match errkind {
                ErrorKind::Interrupted => panic!("Interrupted IO!"),
                ErrorKind::UnexpectedEof => {
                    println!("Reached EOF!");
                    break;
                }
                _ => panic!("Unexpected Error!"),
            },
        };
        if metadata.ip_type() == &[0x08, 0x0] {
            let mut data_length = u16::from_be_bytes(*metadata.length()) as usize;
            if data_length >= 8 {
                data_length -= 8;

                if data_length as usize == QUOTE_PACKET_SIZE {
                    quote_packet_count += 1;
                    let quote_packet = match get_quote_packet(&mut buffer) {
                        Ok(res) => res,
                        Err(errkind) => match errkind {
                            ErrorKind::Interrupted => panic!("Interrupted IO!"),
                            ErrorKind::UnexpectedEof => {
                                println!("Reached EOF!");
                                return;
                            }
                            _ => panic!("Unexpected Error!"),
                        },
                    };
                    if should_reorder {
                        add_quote_packet_to_priority_queue(
                            &mut quote_packet_priority_queue,
                            quote_packet.clone(),
                            UtcTime::from(u64::from(pcap.ts_sec())),
                        );
                    } else {
                        println!(
                            "{} {}",
                            String::from(UtcTime::from(u64::from(pcap.ts_sec()))),
                            String::from(&*quote_packet)
                        );
                    }
                } else {
                    let mut dummy = Vec::new();
                    dummy.resize(data_length, 0);
                    buffer.read_exact(dummy.as_mut()).unwrap();
                }
            }
        } else {
            // STP? try and skip 46 bytes then
            let mut data = Vec::new();
            data.resize(46, 0);
            buffer.read_exact(data.as_mut()).unwrap();
        }
        packet_count += 1;
    }
    println!("parsed {} packets", packet_count);
    println!("parsed {} quote packets", quote_packet_count);

    if should_reorder {
        println!("printing reordered packets");
        while !quote_packet_priority_queue.is_empty() {
            if let Some(((quote_packet, utc_time), _priority)) = quote_packet_priority_queue.pop() {
                println!(
                    "{} {}",
                    String::from(&utc_time),
                    String::from(&*quote_packet)
                );
            }
        }
    }
}
